BITS 16

[org 0x7C00]

mov bx, HELLO
call print
call print_nl

mov bx, GOODBYE
call print
call print_nl

jmp $
    
%include "Print.nasm"


HELLO:
    db "Hello World!", 0

GOODBYE:
    db "Goodbye World!", 0

times 510 - ($-$$) db 0

dw 0xAA55
